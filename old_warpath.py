"""
utilities for warping path
"""
from __future__ import division
import copy
import numpy as np
import matplotlib.pyplot as plt

def find_dup_ind(seq):
    """seq is expected to be the first col in warping path."""
    uniq_ind, start_ind, count = np.unique(seq, return_counts=True, \
                                           return_index=True)
    dup_start_ind = start_ind[count > 1]
    dup_end_ind = dup_start_ind + count[count > 1] - 1

    return dup_start_ind, dup_end_ind

def replace_slice(seq, slice_start, slice_end):
    """
    -INPUT
        seq: input seq, expected to be warped F0
        slice_start: start indices of duplicated slices
        slice_ind: end indices of duplicated slices
    """
    seq = seq.astype(float)
    seq_copy = copy.copy(seq)
    bool_new_seq = np.ones(len(seq_copy), dtype=bool)

    for i, j in zip(slice_start, slice_end):
        bool_new_seq[i+1: j+1] = False
        new_val = np.mean(seq_copy[i: j+1])
        seq_copy[i] = new_val

    seq_new = seq_copy[bool_new_seq]
    return seq_new

def gen_seq_acc_wp(seq, warpath):
    """
    To generate a new sequence according to the derived warping path.
    -INPUT
        seq: corresp. col2 in wp
        warpath: warping path
    -OUTPUT
        seq_new: new seq acc. wp
    NOTE: seq is assumed to be ONE-DIM (i.e., F0).
    """
    start_ind, end_ind = find_dup_ind(warpath[:, 0])
    seq_wp = seq[warpath[:, 1]] # warped seq
    seq_new = replace_slice(seq_wp, start_ind, end_ind)
    return seq_new

def visualize_f0_aln(matplot_ax, x_f0, y_f0, warpath, aln_f0, height_diff=10, line_interval=50, \
                     title=None):
    """visualize alignment with f0"""
    # subsetting wp accord. to the given line_interval
    wp_subset_ind = np.array([i for i in range(warpath.shape[0]) \
                          if i % line_interval == 0])
    wp_subset = warpath[wp_subset_ind]
    y_f0_for_plot = y_f0 + height_diff

    # title the plot if given
    if title:
        matplot_ax.set_title(title)
    # plot the two f0 sequences
    matplot_ax.plot(x_f0, lw=1.5, c='r')
    matplot_ax.plot(y_f0_for_plot, lw=1.5, c='g')
    # plot the association/alignment between the seqs
    for i in wp_subset:
        matplot_ax.plot((i[0], i[1]), (x_f0[i[0]], y_f0_for_plot[i[1]]), c='k')
    # plot the generated/resynthesized new f0 accord. alignment
    #if aln_f0:
    matplot_ax.plot(aln_f0 + height_diff, lw=2, c='b')

    

    


    

    

    